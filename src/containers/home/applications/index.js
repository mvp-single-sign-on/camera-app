import styled from 'styled-components'
import React from 'react'
import { AppItem } from 'components/application-item'

const AppWrapper = styled.div`
margin-left:151px;
margin-right:151px;
margin-top:136px;
padding:32px;
background:red;
width:100%;
height:100%;
background:#FAFBFB;
display: flex;
flex-wrap: wrap;
`

const data = [
  {
    logo: '/images/app-icon/collecting.svg',
    name: 'Collecting',
    description: 'connect any data'
  },
  {
    logo: '/images/app-icon/camera.svg',
    name: 'Camera',
    description: 'streaming camera'
  },
  {
    logo: '/images/app-icon/control.svg',
    name: 'Control',
    description: 'control data station'
  },
  {
    logo: '/images/app-icon/report.svg',
    name: 'Report',
    description: 'build, and export report'
  },
  {
    logo: '/images/app-icon/billing.svg',
    name: 'Billing',
    description: 'invoice, payment'
  },
  {
    logo: '/images/app-icon/incident.svg',
    name: 'Incidents',
    description: 'critical of the system'
  }
]

const ApplicationsArea = (Component) => {
  return <AppWrapper>
    {
      data.map(app => <AppItem onClick={() => { window.location.href = "https://google.com" }} logo={app.logo} name={app.name} description={app.description} />)
    }
    <AppItem />
  </AppWrapper>

}

export { ApplicationsArea }
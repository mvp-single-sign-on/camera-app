import styled from 'styled-components'
import React from 'react'


const MenuWrapper = styled.div`
position: absolute;
width: 252px;
height: 900px;
left: 0px;
top: 0px;

background: #FFFFFF;
/* Small Shadow */

box-shadow: 0px 4px 8px rgba(26, 26, 26, 0.2);
`
const SideBarMenu = () => {
  return <MenuWrapper>
    menu items
  </MenuWrapper>
}

export { SideBarMenu }
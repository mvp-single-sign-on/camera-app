import React from 'react'
import styled from 'styled-components'

const AppItemWrapper = styled.div`
  display:flex;
  background: #FFFFFF;
  box-shadow: 0px 1px 0px rgba(212, 212, 212, 0.25);
  margin: 32px;
  height: 88px;
  width: 358px;
  justify-content: space-between;
  /* justify-content: */
  .left-side{
    display:flex;
    align-items: center;
    /* margin: 32px;
    height: 88px;
    width: 358px; */
    border-radius: 0px;
    /* background: #FFFFFF;
    box-shadow: 0px 1px 0px rgba(212, 212, 212, 0.25); */

    .app-name{
      font-family: Mulish;
      font-style: normal;
      font-weight: bold;
      font-size: 18px;
      line-height: 24px;
    }
    .app-description{
      font-family: Mulish;
      font-style: normal;
      font-weight: normal;
      font-size: 16px;
      line-height: 24px;
    }
  }
  img{
    margin-right:35px;
  }
`



const AppItem = ({ logo, name, description, onClick }) => {
  return <AppItemWrapper>
    <div className='left-side'>
      <img style={{ margin: '28px' }} src={logo} alt="icon" />
      <div>
        <div className='app-name'>{name}</div>
        <div className='app-description'>{description}</div>
      </div>
    </div>

    <img onClick={onClick} src="/images/app-icon/arrow-right.svg" alt="detail icon" />

  </AppItemWrapper>

  // return <div>
  //   <img src={logo} alt="icon" />
  //   <div>{name}</div>
  //   <div>{description}</div>
  // </div>
}

export { AppItem }